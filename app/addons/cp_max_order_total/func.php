<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_cp_max_order_total_pre_add_to_cart ($product_data, $cart, $auth, $update){

	$max_order_total = db_get_field("SELECT max_order_total FROM ?:users WHERE user_id = ?i", $auth['user_id']);

	foreach ($product_data as $data) {

		$product_price = db_get_field("SELECT price FROM ?:product_prices WHERE product_id = ?i", $data['product_id']);
		$total_price = $product_price*$data['amount'];
	}	

	if (empty($cart['total'])) {
		$total = $total_price;

	} else {
		$total = $cart['total'] + $total_price;
	}

	if ($total >= $max_order_total)  {

		fn_set_notification('E', __('error'), __('incorrect_max_order_total'));
		exit;
	}
}

