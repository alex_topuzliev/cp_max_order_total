<div class="control-group profile-field">
	<label for="max_order_total" class="control-label">{__("max_order_total")}:</label>

	<div class="controls">
    	<input type="text" name="user_data[max_order_total]" id="max_order_total" value="{$user_data.max_order_total|default:"0.00"|fn_format_price:$primary_currency:null:false}" size="40" class="input" />
	</div>
</div>